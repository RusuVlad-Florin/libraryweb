# Documentatie Library




## Descriere Proiect
Proiectul doreste sa implementeze un produs software in Spring Boot.
Tema proiectului este realizarea unei aplicatii web pentru un ONG. Eu fac parte dintr-un cerc de filosofie, iar aici s-a dorit aceasta aplicatie in special pentru a gestiona imprumutul de carti de la biblioteca interna. De aceea, o prima functionalitate doresc sa fie tocmai acest serviciu de imprumutari de carti si gestionarea lor.
Ca si baza de date am folosit Firebase, deoarece am mai lucrat cu el, si cred ca imi ofera scalabilitate mai mare, dorind sa implementez si o varianta mobile dupa aceea.

## Tehnologii folosite:

Dupa ce am discutat la laborator, s-a convenit sa facem mai intai partea de Back-End, astfel momentan am:

- [Spring Boot] - Framework pentru Java!
- [Firebase] - Cloud Firestore pentru date

## Tema 1
Aceasta prezinta un serviciu REST, astfel am implementat conexiunea la Baza de date, si cateva request-uri: GET all the books, POST - book/user, DELETE - book. Cu mentiunea ca am folosit Postman pentru a testa daca sunt in regula implementate si am observat ca in cazul DELETE-ului nu realizeaza stergerea daca numele contine spatiu. Trebuie revizuit.
Fiecare Request este gestionat de DBAPI care realizeaza inserarile in bd, query-uri sau delete.


Structura urmarita este una MVC:
-Modelul: Clasele Book, User, CurrentUser(Momentan aceasta nu face nimic)
-Controller: HelloController- gestioneaza request-urile.
-View: neimplementat inca, desi am cateva fisiere html




## Tema 2
In aceasta tema am avut de implementat Observer Design Pattern. In proiect, voi avea un use case in care userii vor putea sa scrie anunturi vizibile pentru toti. Asa ca am implementat functionalitatea pentru logare, folosind o instanta a CurrentUser(un singleton ce pastreaza referinta pentru userul actual), si cu ajutorul claselor Observable, NewsService si Observer am implementat serviciul de postare de anunturi. Fiecare Obiect observable are o referinta la un user si un Queue de anunturi pentru a putea fi afisate. Cand se doreste postarea unui anunt, toti userii sunt instiintati, retinandu-se o lista cu acestia in NewsService. Mai mult, am adaugat 2 Facade-uri una pentru useri si una pentru books pentru a putea ascunde implementarea in aceste clase.


# Diagrama Observer
![diagrama](https://bitbucket.org/RusuVlad-Florin/libraryweb/raw/52d400bc2dc6163e7ad082f0984eb240dcc4b964/diagrama.png)


## Tema 3
Aceasta tema prezinta implementarea unui Design Pattern la alegere dintre Factory si Strategy. Eu am ales sa lucrez cu Factory deoarece am putut mai usor sa il integrez in proiect
### **Ce am facut mai exact:** 
- Am creat o clasa Administrator, pe langa cea de User
- O clasa Person le va fi superclasa celor 2, pentru a putea gestiona design pattern-ul
- Clasa PersonFactory, prin metoda create si primul parametru va decide ce constructor sa apeleze: unul din clasa User sau unul din clasa Administrator. 

# Diagrama de secventa:
### Aici am reprezentat flow-ul unui GET de carti din baza de date.
![diagrama de secventa](https://bitbucket.org/RusuVlad-Florin/libraryweb/raw/b304145cd72f5afb2b45830932fba808ca0baab6/sequenceDiagram.png)
## Mentiuni
Acest proiect l-am construit pe scheletul altui proiect personal, de aceea mai sunt linii comentate, clase neimplementate si fisiere view.
Proiectul pe care l-am modificat este
https://libraryna.herokuapp.com/