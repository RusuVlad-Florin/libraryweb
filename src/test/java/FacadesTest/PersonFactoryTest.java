package FacadesTest;

import com.library.vlad.Model.Administrator;
import com.library.vlad.Model.Person;
import com.library.vlad.Model.PersonFactory;
import com.library.vlad.Model.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
@ExtendWith(MockitoExtension.class)

public class PersonFactoryTest {

    PersonFactory personFactory= new PersonFactory();


@Test
    public void testCreateUser()
    {
        Person p=personFactory.create("User","test","test","test");
        System.out.println(p.getClass());
        assertEquals(true,p instanceof User);
    }


    @Test
    public void testCreateAdmin()
    {
        Person p=personFactory.create("Administrator","test","test","test");
        System.out.println(p.getClass());
        assertEquals(true,p instanceof Administrator);
    }


}
