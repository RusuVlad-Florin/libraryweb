package FacadesTest;

import com.library.vlad.DB.DBService;
import com.library.vlad.Facades.UserServiceFacade;
import com.library.vlad.Model.CurrentUser;
import com.library.vlad.Model.NewsService;
import com.library.vlad.Model.User;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserFacadeTest {
    @Mock
    private DBService dbService = mock(DBService.class);

    @Mock
    private NewsService newsService = mock(NewsService.class);


    @Test
    public void testLogin() {
        UserServiceFacade userServiceFacade = new UserServiceFacade();
        userServiceFacade.setDbService(dbService);

        assertEquals(userServiceFacade.login("qwe@gmail.com", "qwertyu"), CurrentUser.getInstance().getUser());

    }

    /***
     * this one shall return null
     */
    @Test
    public void testLoginFailed() {
        UserServiceFacade userServiceFacade = new UserServiceFacade();
        userServiceFacade.setDbService(dbService);

        assertEquals(userServiceFacade.login("qwe@afagmfafail.com", "qwertyafsafau"), null);

    }

    //de intrebat cum sa verific daca se apeleaza metode din dbService
    @Test
    public void testPostUser() throws ExecutionException, InterruptedException {

        UserServiceFacade userServiceFacade = new UserServiceFacade();
        userServiceFacade.setDbService(dbService);
        User u = new User("test", "test", "test");
        userServiceFacade.postUser(u);

        verify(dbService, atLeastOnce()).addDataToCloud(u);
    }
//De intrebat de ce nu afiseaza

    @Test
    public void testGetUsers() throws ExecutionException, InterruptedException {
//        when(dbService.getUsers()).thenReturn()

        UserServiceFacade userServiceFacade = new UserServiceFacade();
        userServiceFacade.setDbService(dbService);
        userServiceFacade.getUsers();

        verify(dbService, atLeastOnce()).getUsers();
        /*ArrayList<User> users = userServiceFacade.getUsers();
        System.out.println(users.size());
        for (User u : users)
            System.out.println(u);
        assertEquals(4, userServiceFacade.getUsers().size());

    */
    }

    @Test
    public void deletetest() throws ExecutionException, InterruptedException {
        UserServiceFacade userServiceFacade = new UserServiceFacade();
        userServiceFacade.setDbService(dbService);
        userServiceFacade.deleteUser("asdasda");
        verify(dbService, atLeastOnce()).deleteUser("asdasda");
//        assertEquals(true, userServiceFacade.deleteUser("asdasda"));

    }

    @Test
    public void publishAnn() {
        UserServiceFacade userServiceFacade = new UserServiceFacade();
        userServiceFacade.setDbService(dbService);
        userServiceFacade.setNewsService(newsService);
        userServiceFacade.publishAnnouncement("Anunt");

        verify(newsService, atLeastOnce()).addAnnouncement("Anunt");

    }

}
