package FacadesTest;


import com.library.vlad.DB.DBService;
import com.library.vlad.Facades.BookServiceFacade;
import com.library.vlad.Facades.UserServiceFacade;
import com.library.vlad.Model.Book;
import com.library.vlad.Model.CurrentUser;
import com.library.vlad.Model.NewsService;
import com.library.vlad.Model.User;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

/***
 * trebuie intrebat de ce nu imi initializeaza dbService daca ii dau mock.
 */
@ExtendWith(MockitoExtension.class)
public class BookFacadeTest {

    @Mock
    private DBService dbService = Mockito.mock(DBService.class);


    @Test
    public void testAddBook() throws ExecutionException, InterruptedException {
        BookServiceFacade bookServiceFacade = new BookServiceFacade();
        bookServiceFacade.setDbService(dbService);
        Book b = new Book("Cel mai iubit dintre pamanteni", "Marin Preda");
        bookServiceFacade.postBook(b);
        verify(dbService, atLeastOnce()).addDataToCloud(b);

    }

    //de intrebat!!!
    @Test
    public void testGetBooks() throws ExecutionException, InterruptedException {
        BookServiceFacade bookServiceFacade = new BookServiceFacade();
        bookServiceFacade.setDbService(dbService);

        bookServiceFacade.getBooks();
        verify(dbService,atLeastOnce()).getBooks();
        assertEquals(0, bookServiceFacade.getBooks().size());

    }
}
