import com.library.vlad.Controller.HelloController;
import com.library.vlad.DB.FBInitialize;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/***
 * Am prezentat aici teste pentru cele 3 operatii:get,post,delete
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class RESTTest  {
    private MockMvc mockMvc;
@InjectMocks
private HelloController helloController;
@Before
public void setup()
{
    new FBInitialize().initialize();
    mockMvc= MockMvcBuilders.standaloneSetup(helloController).build();

}

    @Test
    public void testPost() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/addBook?name=Republica&author=Platon"))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    public void testDelete() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/deleteBook?name=Republica"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Found and deleted"));

    }
    @Test
    public void testDeleteNotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/deleteBook?name=Republica"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Not Found"));

    }

@Test
public void testGet() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get("/findBook"))
            .andExpect(MockMvcResultMatchers.status().isOk());

}

}
