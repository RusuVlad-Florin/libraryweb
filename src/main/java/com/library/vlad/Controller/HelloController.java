package com.library.vlad.Controller;

import com.google.firebase.auth.FirebaseAuthException;
import com.library.vlad.DB.DBService;
import com.library.vlad.DB.UsersRepo;
import com.library.vlad.Facades.BookServiceFacade;
import com.library.vlad.Facades.UserServiceFacade;
import com.library.vlad.Model.*;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/***
 * Controller for the REST service:-> get, post, delete operation for the moment
 */
@RestController
public class HelloController {
    BookServiceFacade bookFacade=new BookServiceFacade();
    UserServiceFacade userFacade=new UserServiceFacade();
    PersonFactory personFactory=new PersonFactory();
//    @GetMapping("/addBook")
//    public String addBookForm(Model model) {
//        model.addAttribute("book", new Book());
//        return "addBook";
//    }
//
//
//    @PostMapping("/addBook")
//    public String addBookSubmit(@ModelAttribute Book book, Model model) throws ExecutionException, InterruptedException {
//        model.addAttribute("book", book);
//        bookService.addDataToCloud(book);
//        System.out.println(book.getName());
//        return "home";
//    }
//
//
//    @GetMapping("/register")
//    public String addUser( Model model)
//    {
//        model.addAttribute("user",new User());
//
//        return "register";
//    }
//    @PostMapping("/register")
//    public String register(@ModelAttribute User user,Model model) throws ExecutionException, InterruptedException {
//        model.addAttribute("user",user);
//        CurrentUser.setUser(user);
//        bookService.addDataToCloud(user);
//        return "home";
//    }
//
//    @GetMapping("/login")
//    public String loginUser( Model model)
//    {
//        model.addAttribute("user",new User());
//
//        return "login";
//    }
//    @PostMapping("/login")
//    public String login(@ModelAttribute User user,Model model) throws ExecutionException, InterruptedException {
//        model.addAttribute("user",user);
//        CurrentUser.setUser(user);
//        System.out.println(bookService.findUser(user.getEmail(),user.getPass()));
//
//        return "home";
//    }
//
//
//    @GetMapping("/showBooks")
//    public String showBooks(Model model) throws ExecutionException, InterruptedException {
//        ArrayList<Book> books = bookService.getBooks();
//        model.addAttribute("books", books);
//        return "showBooks";
//
//    }
//
//    @RequestMapping("/")
//    public String index() throws ExecutionException, InterruptedException {
//
//        return "home";
//    }

    @PostMapping("/addBook")
    @ResponseBody
    public String addBookSubmit(@RequestParam String name, @RequestParam String author) throws ExecutionException, InterruptedException, FirebaseAuthException {
        bookFacade.postBook(new Book(name, author));
        return "addBook";
    }

    @DeleteMapping("/deleteBook")
    @ResponseBody
    public String DeleteBook(@RequestParam String name) throws ExecutionException, InterruptedException, FirebaseAuthException {
        if (bookFacade.deleteBook(name))
            return "Found and deleted";
        return "Not Found";

    }
    @PostMapping("/addAdmin")
    @ResponseBody
    public String addAdmin(@RequestParam String name, @RequestParam String email, @RequestParam String password) throws ExecutionException, InterruptedException, FirebaseAuthException {
        Person p=personFactory.create( "Administrator",name, email, password);

        userFacade.postUser(p);

        return "Administrator "+name+" added";
    }


    @PostMapping("/addUser")
    @ResponseBody
    public String addUser(@RequestParam String name, @RequestParam String email, @RequestParam String password) throws ExecutionException, InterruptedException, FirebaseAuthException {
        Person p=personFactory.create( "User",name, email, password);

        userFacade.postUser(p);

        return "User "+name+" added";
    }

    @GetMapping("/findBook")
    public ArrayList<Book> findAllBooks(Model model) throws ExecutionException, InterruptedException {
        return bookFacade.getBooks();

    }

    @PostMapping("/login")
    @ResponseBody
    public Person loginUser(@RequestParam String email, @RequestParam String password) throws ExecutionException, InterruptedException {
        return userFacade.login(email, password);

    }

    @PostMapping("/postAnnouncement")
    @ResponseBody
    public void postMessage(@RequestParam String message) {
        userFacade.publishAnnouncement(message);

    }

    @PostMapping("/loout")
    @ResponseBody
    public void logout() {
        userFacade.logoutUser();

    }

}