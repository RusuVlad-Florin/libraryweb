package com.library.vlad.DB;

import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.cloud.FirestoreClient;

public class FireBaseService {
    private Firestore dbFirestore;
    private CollectionReference colRef = null;

    public FireBaseService() {
        dbFirestore = FirestoreClient.getFirestore();
    }

    CollectionReference getBooksReference()
    {
        colRef =  dbFirestore.collection("Books");
        return colRef;


    }

    CollectionReference getUsersReference()
    {
        colRef =  dbFirestore.collection("Books");
        return colRef;
    }

    public Firestore getDbFirestore() {
        return dbFirestore;
    }

    public CollectionReference getColRef() {
        return colRef;
    }
}
