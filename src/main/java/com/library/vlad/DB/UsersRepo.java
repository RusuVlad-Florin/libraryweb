package com.library.vlad.DB;

import com.library.vlad.Model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class UsersRepo {
    private List<User> users=new ArrayList<User>();

    public UsersRepo() throws ExecutionException, InterruptedException {
        users=new DBService().getUsers();
    }

    public List<User> getUsers() {
        return users;
    }

    public void addUser(User u)
    {
        users.add(u);

    }
}
