package com.library.vlad.DB;

import com.google.cloud.firestore.*;
import com.library.vlad.Model.*;
import com.google.api.core.ApiFuture;
import com.google.firebase.cloud.FirestoreClient;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;


@Service
public class DBService {
 PersonFactory personFactory=new PersonFactory();

    /***
     * Adds to database either a Book, either an user, it depends on the object sent, that's why I made it generic
     * @param t
     * @param <T>
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public <T> void addDataToCloud(T t) throws ExecutionException, InterruptedException {

        Firestore dbFirestore = FirestoreClient.getFirestore();
        DocumentReference docRef = null;
        if (t instanceof Book) {

            docRef = dbFirestore.collection("Books").document(((Book) t).getName());
        } else if (t instanceof User) {
            docRef = dbFirestore.collection("Users").document(((User) t).getEmail());
        }
        else if (t instanceof Administrator)
        {
            docRef = dbFirestore.collection("Administrators").document(((Administrator) t).getEmail());
        }

        ApiFuture<WriteResult> result = docRef.set(t);
        System.out.println("Update time : " + result.get().getUpdateTime());

    }

    /***
     *
     * @return All the books from database
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public ArrayList<Book> getBooks() throws ExecutionException, InterruptedException {
        // asynchronously retrieve all books
        ArrayList<Book> books = new ArrayList<Book>();
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<QuerySnapshot> query = dbFirestore.collection("Books").get();
        QuerySnapshot querySnapshot = query.get();
        List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();
        for (QueryDocumentSnapshot document : documents) {
            books.add(new Book(document.getString("name"), document.getString("author")));
        }

        return books;
    }


    public ArrayList<User> getUsers() throws ExecutionException, InterruptedException {
        // asynchronously retrieve all books
        ArrayList<User> users = new ArrayList<User>();
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<QuerySnapshot> query = dbFirestore.collection("Users").get();
        QuerySnapshot querySnapshot = query.get();
        List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();
        for (QueryDocumentSnapshot document : documents) {
            users.add((User)personFactory.create("User",document.getString("name"), document.getString("email"), document.getString("pass")));
        }

        return users;
    }

    /***
     * Queries for the book with the specific name and deletes it, if found
     * @param name
     * @return true if found, else false
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public boolean deleteBook(String name) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        CollectionReference books = dbFirestore.collection("Books");
        Query query = books.whereEqualTo("name", name);
        ApiFuture<QuerySnapshot> querySnapshot = query.get();
        if (querySnapshot != null) {
            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                document.getReference().delete();
                return true;

            }
        }
        return false;
    }

    /***
     * It deletes an user from db
     * @param name
     * @return true if done successfully, false else
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public boolean deleteUser(String name) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        CollectionReference books = dbFirestore.collection("Users");
        Query query = books.whereEqualTo("name", name);
        ApiFuture<QuerySnapshot> querySnapshot = query.get();
        if (querySnapshot != null) {
            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                document.getReference().delete();
                return true;

            }
        }
        return false;
    }


    /***
     * This function makes the login, and the currentUser will now have refference to the user logged in
     *
     * @param email
     * @param password
     * @return true if the login was successful
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public boolean loginUser(String email, String password) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        CollectionReference users = dbFirestore.collection("Users");
        Query query = users.whereEqualTo("email", email).whereEqualTo("pass", password);
        ApiFuture<QuerySnapshot> querySnapshot = query.get();

        if (querySnapshot != null) {
            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                CurrentUser.getInstance().setUser(personFactory.create("User",document.getString("name"), document.getString("email"), document.getString("pass")));
                return true;

            }

        }
        return false;
    }

    /***
     * Used for logout , the currentUser instance will have the user field null
     */
    public void logoutUser() {
        CurrentUser.getInstance().logout();

    }


}