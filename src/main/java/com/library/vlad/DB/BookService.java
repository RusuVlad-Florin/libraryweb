package com.library.vlad.DB;

import com.google.cloud.firestore.*;
import com.library.vlad.Model.*;
import com.google.api.core.ApiFuture;
import com.google.firebase.cloud.FirestoreClient;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;


@Service
public class BookService {

    public static final String COL_NAME = "users";

    /***
     * Adds to database either a Book, either an user, it depends on the object sent, that's why I made it generic
     * @param t
     * @param <T>
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public <T> void addDataToCloud(T t) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        DocumentReference docRef = null;
        if (t instanceof Book) {

            docRef = dbFirestore.collection("Books").document(((Book) t).getName());
        } else if (t instanceof User) {
            docRef = dbFirestore.collection("Users").document(((User) t).getEmail());
        }

        ApiFuture<WriteResult> result = docRef.set(t);
        System.out.println("Update time : " + result.get().getUpdateTime());

    }

    /***
     *
     * @return All the books from database
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public ArrayList<Book> getBooks() throws ExecutionException, InterruptedException {
        // asynchronously retrieve all books
        ArrayList<Book> books = new ArrayList<Book>();
        Firestore db = FirestoreClient.getFirestore();
        ApiFuture<QuerySnapshot> query = db.collection("Books").get();
        QuerySnapshot querySnapshot = query.get();
        List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();
        for (QueryDocumentSnapshot document : documents) {
            books.add(new Book(document.getString("name"), document.getString("author")));


        }

        return books;
    }

    /***
     * Queries for the book with the specific name and deletes it, if found
     * @param name
     * @return true if found, else false
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public boolean deleteBook(String name) throws ExecutionException, InterruptedException {
        Firestore db = FirestoreClient.getFirestore();
        CollectionReference books = db.collection("Books");
        Query query = books.whereEqualTo("name", name);
        ApiFuture<QuerySnapshot> querySnapshot = query.get();
        if (querySnapshot != null) {
            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                document.getReference().delete();
                return true;

            }
        }
return false;
    }

    /***
     * It is used for the login part, but for the moment it is not used
     * @param email
     * @param password
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public User findUser(String email, String password) throws ExecutionException, InterruptedException {
        Firestore db = FirestoreClient.getFirestore();
        CollectionReference users = db.collection("Users");
        Query query = users.whereEqualTo("email", email).whereEqualTo("pass", password);
        ApiFuture<QuerySnapshot> querySnapshot = query.get();

        if (querySnapshot != null) {
            for (DocumentSnapshot document : querySnapshot.get().getDocuments()) {
                return new User(document.getString("name"), document.getString("email"), document.getString("pass"));
            }

        }
        return null;


    }
}