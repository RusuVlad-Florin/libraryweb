package com.library.vlad.Facades;

import com.library.vlad.DB.DBService;
import com.library.vlad.Model.Book;
import com.library.vlad.Model.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/***
 * This function represents a facade for the book service, it will provide all the methods
 * of access to the database
 */
@Component
public class BookServiceFacade {
    private DBService dbService = new DBService();

    public void postBook(Book u) {
        try {
            dbService.addDataToCloud(u);
        } catch (ExecutionException e) {
            e.printStackTrace();
            System.out.println("Can't save user " + u.getName() + " to database!!!");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void setDbService(DBService dbService) {
        this.dbService = dbService;
    }

    public boolean deleteBook(String name) {
        try {
            return dbService.deleteBook(name);
        } catch (ExecutionException e) {
            e.printStackTrace();
            System.out.println("Error deleting the book " + name);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    public ArrayList<Book> getBooks() {
        try {
            return dbService.getBooks();
        } catch (ExecutionException e) {
            e.printStackTrace();
            System.out.println("Error getting all the books from db");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }


}
