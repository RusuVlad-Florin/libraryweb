package com.library.vlad.Facades;

import com.library.vlad.DB.DBService;
import com.library.vlad.Model.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/***
 * This class represents a facade for users, providing methods for
 * access to database and the implementation for the observer pattern
 *
 */
@Component
public class UserServiceFacade {

    private DBService dbService = new DBService();

    private NewsService newsService = new NewsService();
    private List<Observable> observables = new ArrayList<Observable>();

    public DBService getDbService() {
        return dbService;
    }

    public void setDbService(DBService dbService) {
        this.dbService = dbService;
    }
    public void setNewsService(NewsService newsService)
    {
        this.newsService=newsService;
    }

    public void postUser(Person u) {
        try {
            dbService.addDataToCloud(u);
        } catch (ExecutionException e) {
            e.printStackTrace();
            System.out.println("Can't save user " + u.getName() + " to database!!!");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public NewsService getNewsService() {
        return newsService;
    }


    public ArrayList<User> getUsers() {
        try {
            return dbService.getUsers();
        } catch (ExecutionException e) {
            System.out.println("Couldn't get all the users from DB");
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Person login(String email, String password) {
        try {
            if (dbService.loginUser(email, password))
                return CurrentUser.getInstance().getUser();

        } catch (ExecutionException e) {
            e.printStackTrace();
            System.out.println("Problema la logare!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void logoutUser() {

        dbService.logoutUser();
    }

    public void publishAnnouncement(String str) {
        if (observables.size() == 0) {

            ArrayList<User> users = getUsers();
            for (User u : users) {
                Observable obs = new Observable(u);
                this.getNewsService().addObservable(obs);

            }
            newsService.addAnnouncement(str);


        } else
            newsService.addAnnouncement(str);
    }

    public boolean deleteUser(String name) {
        try {
            return dbService.deleteUser(name);

        } catch (ExecutionException e) {

            System.out.println("error while deleting user" + name);
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

}
