package com.library.vlad.Model;

public interface Observer {
public void addObservable(Observable o);
public void removeObservable(Observable o);
public void addAnnouncement(String str);


}
