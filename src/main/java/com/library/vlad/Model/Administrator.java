package com.library.vlad.Model;

import lombok.Getter;
import lombok.Setter;

/***
 * Aceasta clasa va putea manageria userii, dar si cartile
 */
@Getter
@Setter
public class Administrator extends Person{

    public Administrator (String name, String email, String pass)
    {
        this.setEmail(email);
        this.setName(name);
        this.setPass(pass);
    }


}
