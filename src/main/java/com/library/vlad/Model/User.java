package com.library.vlad.Model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
@Getter
@Setter
public class User extends Person {
    private ArrayList<Book> borrowedBooks;

    @Override
    public String toString() {
        return "User{" +
                "name='" + super.getName() + '\'' +
                ", email='" + this.getEmail() + '\'' +
                ", pass='" + this.getPass() + '\'' +
                '}';
    }

    public User(String name, String email, String pass) {
        super();
        this.setName(name);
        this.setEmail (email);
        this.setPass(pass);
    }

    public User(String name) {
        this.setName( name);
        borrowedBooks = new ArrayList<Book>();
    }

    public User() {
    }

    public ArrayList<Book> getBorrowedBooks() {
        return borrowedBooks;
    }

    public void setBorrowedBooks(ArrayList<Book> borrowedBooks) {
        this.borrowedBooks = borrowedBooks;
    }
}
