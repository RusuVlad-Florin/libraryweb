package com.library.vlad.Model;

/***
 *   This class implements the Factory Design Pattern, creating
 *Users or Administrators.
 */
public class PersonFactory {

    public Person create(String type,String name, String email, String pass)
    {
        if(type.equals("User"))
            return new User(name,email,pass);
        else
        if(type.equals("Administrator"))
            return new Administrator(name,email,pass);
        else
            return null;
    }




}
