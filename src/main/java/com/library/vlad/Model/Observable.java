package com.library.vlad.Model;

import java.util.PriorityQueue;
import java.util.Queue;

public class Observable {
    private User user;
    private Queue<String> announcements=new PriorityQueue<String>() ;
    public Observable(User u)
    {
        user=u;
    }
    public void update(String ann)
    {
        System.out.println(user.getName()+ ann);
        announcements.add(ann);

    }
}
