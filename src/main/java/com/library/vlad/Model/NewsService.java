package com.library.vlad.Model;


import java.util.ArrayList;
import java.util.List;

public class NewsService implements Observer {
    private List<Observable> users = new ArrayList<Observable>();

    @Override
    public void addObservable(Observable u) {
        users.add(u);
    }


    @Override
    public void removeObservable(Observable u) {
        users.remove(u);
    }

    public void addAnnouncement(String ann) {
        for (Observable o : users) {
            o.update(ann);
        }
    }

}
