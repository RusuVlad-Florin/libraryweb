package com.library.vlad.Model;

public class CurrentUser {
    private static CurrentUser instance = null;
    private Person user;

    private CurrentUser() {
        user = new User();
        user = null;
    }

    public boolean isLoggedOn() {
        if (user != null)
            return true;
        return false;

    }

    public void logout() {
        this.user = null;

    }

    public boolean isNull() {
        if (getInstance().user == null)
            return true;
        return false;
    }

    public static CurrentUser getInstance() {
        if (instance == null)
            instance = new CurrentUser();
        return instance;
    }

    public Person getUser() {
        return user;
    }

    public static void setUser(Person u) {
        CurrentUser.instance.user = u;
    }
}
